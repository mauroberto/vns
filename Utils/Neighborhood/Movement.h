#ifndef MOVEMENT_H
#define MOVEMENT_H

#include "Solution.h"
#include "Instance.h"


//Crie um movimento para cada vizinhança. O método move recebe uma solução e aplica o movimento nessa solução.
class Movement{
private:
  virtual bool isEqual(const Movement& other) const{
    return false;
  };
public:
  Instance* ins;
  Movement();
  Movement(Instance* ins);
  virtual ~Movement();
  virtual Solution move(const Solution & sol);
  virtual Movement* clone() const {
    return NULL;
  };



  //Comparação dos movimentos. Faz sentido quando se usa busca tabu
  bool operator==(const Movement& other) const{
    return typeid(*this) == typeid(other) && isEqual(other);
  }

  Movement& operator=(const Movement& other){
    ins = other.ins;
    return *this;
  }
};

#endif
