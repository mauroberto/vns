#include "AddItemMovement.h"
#include <iostream>

AddItemMovement::AddItemMovement(){}


AddItemMovement::AddItemMovement(Instance* ins, int add) : Movement(ins){
    this->ins = ins;
    this->add = add;
}


AddItemMovement::~AddItemMovement(){}


Solution AddItemMovement::move(const Solution & sol){
    Solution sol2(sol);
    sol2.addFirstFit(this->ins->ads[this->add]);
    return sol2;
}

Movement* AddItemMovement::clone() const{
    return new AddItemMovement(this->ins, this->add);
}
