#ifndef ADD_ITEM_MOVEMENT_H
#define ADD_ITEM_MOVEMENT_H

#include "Movement.h"

class AddItemMovement : public Movement{
private:
    int add;

    virtual bool isEqual(const Movement& other) const{
        AddItemMovement aic = static_cast<const AddItemMovement&>(other);
        return add == aic.add;
    }
public:
    AddItemMovement();
    AddItemMovement(Instance* ins, int add);
    ~AddItemMovement() override;
    Solution move(const Solution & sol) override;
    virtual Movement* clone() const override;

    //A coparação de movimentos é útil na busca tabu
    virtual AddItemMovement& operator=(const AddItemMovement& other){
        Movement::operator =(other);
        add = other.add;
        return *this;
    }
};

#endif
