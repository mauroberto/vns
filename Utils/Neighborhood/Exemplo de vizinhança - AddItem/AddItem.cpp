#include "AddItem.h"
#include <iostream>
#include <algorithm>

AddItem::AddItem(){

}

AddItem::AddItem(Instance* instance) : Neighborhood(ins){
  this->ins = instance;
}

AddItem::~AddItem(){
}

void AddItem::generateNeighbors(Solution & sol){
  for (std::vector< Movement* >::iterator it = this->neighbors.begin() ; it != this->neighbors.end(); ++it){
    delete (*it);
  }

  this->neighbors.clear();

  this->max = sol;
  Solution sol2 = sol;
  for(int i=0; i<this->ins->A; i++){
    if(!sol2.y[i] && sol2.addFirstFit(this->ins->ads[i])){
      this->neighbors.push_back(new AddItemMovement(this->ins, i));
      if(max < sol2){
	this->max = sol2;
      }
      sol2 = sol;
    }
  }
}

void AddItem::bestImprovement(Solution & sol){
  for(int k = 0; k < this->ins->A; k++){
    int i = this->ins->ranking[k].second;
    if(!sol.y[i]){
      sol.addFirstFit(this->ins->ads[i]);
    }
  }
}
