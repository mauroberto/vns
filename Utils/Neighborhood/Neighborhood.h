#ifndef NEIGHBORHOOD_H
#define NEIGHBORHOOD_H

#include "Solution.h"
#include "Instance.h"
#include "Utils.h"
#include "Movement.h"


//Toda vizinhança deve extender essa classe
class Neighborhood{
public:
  Instance* ins;
  Utils utils;
  Solution max;
  std::vector<Movement*> neighbors; //lista de movimentos. Se as soluções forem pequenas, compensa armazenar a solução ao invés do movimento
  
  Neighborhood();
  Neighborhood(Instance* ins);
  virtual ~Neighborhood();

  //todos os retornos são feitos por referência (modificando sol)
	
  void maxNeighborhood(Solution & sol); //devole o melhor vizinho na lista de neighbors
  bool randomNeighborhood(Solution & sol); //devolve um vizinho aleatório da lista de neighbors
  virtual void generateNeighbors(Solution & sol); //gera a lista de movimentos a partir de sol e salva em max o melhor dos vizinhos
  virtual void bestImprovement(Solution & sol); //faz best improvement em sol
};

#endif
