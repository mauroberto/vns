#ifndef UTILS_H
#define UTILS_H
#include <string>

class Utils {
public:
  Utils();
	~Utils();

  int getRandomNumber(int min, int max);
  std::string getFileName(const std::string& s);
};

#endif
