#ifndef SOLUTION_H
#define SOLUTION_H
#include "Slot.h"
//#include "BITree.h"
#include <vector>
#include <string>
#include "Instance.h"

class Solution {
public:
  double value; //Valor da função objetivo. Não é usado no VNS.
  Instance *ins;

  Solution();
  Solution(Instance * ins);
  Solution(const Solution& sol2); //Para instanciar uma nova solução como cópia de sol2
  ~Solution();

  Solution& operator= (const Solution& sol2);
  bool operator< (const Solution& sol2); //Usado no VNS para comparar as soluções
};

#endif
