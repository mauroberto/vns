#include "Utils.h"
#include <cstdlib>
#include <ctime>

using namespace std;

Utils::Utils(){
  srand(0);
}

Utils::~Utils(){

}

int Utils::getRandomNumber(int min, int max){
  int r = (rand() % (max - min + 1)) + min;
  return r;
}

string Utils::getFileName(const string& s) {
   char sep = '/';

	 #ifdef _WIN32
   	sep = '\\';
	 #endif

   string aux;

   size_t i = s.rfind(sep, s.length());
   if (i != string::npos) {
      aux = s.substr(i+1, s.length() - i);
      i = aux.rfind('.', aux.length());
      if (i != string::npos){
        return (aux.substr(0, i));
      }
   }

   return("");
}