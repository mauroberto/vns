#ifndef VNS_H
#define VNS_H

#include "Solution.h"
#include "Instance.h"
#include "ConstructiveHeuristic.h"
#include "Neighborhood.h"
#include "LocalSearch.h"
#include <vector>
#include <time.h>
#include <sys/time.h>
#include "Movement.h"

class VNS {
private:
  const int K_MAX, N_MAX, timeout;
  std::vector<Neighborhood*> N; 
  std::vector<Neighborhood*> N2;
  LocalSearch* ls;
  struct timeval start, stop;

  void neighborhoodChange(Solution & sol, Solution & sol2, int & k, int & n, bool force_next);
  bool shake(Solution & sol, int n, int k);
  void VND(Solution & sol);
  void neighborhoodChangeVND(Solution & sol, Solution & sol2, int & n);
public:

  VNS();
  VNS(Instance* instance, int K_MAX, int N_MAX, int timeout);
  ~VNS();
  void runVNS(Solution & sol);
};

#endif
