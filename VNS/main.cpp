#include "Solution.h"
#include "Instance.h"
#include "ConstructiveHeuristic.h"
#include <iostream>
#include <time.h>
#include "VNS.h"
#include <string>

using namespace std;

int timeout;
double elapsed;
clock_t clk;

int main(int argc, char* argv[]){
  if (argc < 2) {
    cout << "Usage: ./VNS filename" << endl;
    return 1;
  }

  int K = 6, N = 4;
  clk = clock();
  timeout = 3600;


  double alpha = 0.3;
  
  cout.precision(2);

  Instance ins(argv[1]);
  ConstructiveHeuristic ch(ins);

  Solution sol = ch.greedyRandomizedConstruction3(alpha);

  VNS vns(&ins, K, N, timeout);

  vns.runVNS(sol);

  cout << fixed << "Solution = " << sol.value << endl;
  elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  cout << fixed << "Time = " << elapsed << endl;
  return 0;
}
