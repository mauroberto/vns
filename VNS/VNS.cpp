#include "VNS.h"
#include <iostream>

using namespace std;

VNS::VNS(): K_MAX(1), N_MAX(0), timeout(3600){

}

VNS::VNS(Instance* instance, const int _K_MAX, const int _N_MAX, const int _timeout)
: K_MAX(_K_MAX), N_MAX(_N_MAX), timeout(_timeout){

  gettimeofday(&start, NULL);
  this->ins = instance;

  this->ls = new LocalSearch(this->ins, timeout, start);
  Utils utils;
  //this->ts = new TabuSearch(this->ins, 1, 4, timeout, start);

  this->c.resize(this->N_MAX, 1);
  this->N.resize(this->N_MAX);
  this->N2.resize(this->N_MAX);


  //instanciando as vizinhanças
  this->N[0] = new Move(this->ins);
  this->N[1] = new Repack(this->ins);
  this->N[2] = new AddItem(this->ins);
  this->N[3] = new ChangeItem(this->ins);

  this->N2[0] = new Move(this->ins);
  this->N2[1] = new Repack(this->ins);
  this->N2[2] = new AddItem(this->ins);
  this->N2[3] = new ChangeItem(this->ins);
}

VNS::~VNS(){
  for(int i = 0; i < this->N_MAX; i++){
    delete this->N[i];
    delete this->N2[i];
  }

  delete this->ls;
  delete this->ts;
}

void VNS::neighborhoodChange(Solution & sol, Solution & sol2, int & k, int & n, bool force_next){
  if(sol < sol2){
    k = 1;
    n = 0;
    sol = sol2;
    this->N[n]->generateNeighbors(sol);
  }else{
    k++;
  }

  if(k > this->K_MAX || force_next){
    k = 1;
    n++;
    if(n < this->N_MAX){
      this->N[n]->generateNeighbors(sol);
    }
  }
}

void VNS::neighborhoodChangeVND(Solution & sol, Solution & sol2, int & n){
  if(sol < sol2){
    n = 0;
    sol = sol2;
  }else{
    n++;
  }
}

bool VNS::shake(Solution & sol, int n, int k){
  return this->N[n]->randomNeighborhood(sol);
}

void VNS::VND(Solution & sol){
  int i = 0;
  while(i < this->N_MAX){
    Solution sol2 = sol;
    this->N2[i]->generateNeighbors(sol);
    this->N2[i]->maxNeighborhood(sol2);

    this->neighborhoodChangeVND(sol, sol2, i);
  }
}

void VNS::runVNS(Solution & sol){
  this->c.resize(this->N_MAX, 1);
  
  int n = 0;
  this->N[n]->generateNeighbors(sol);
  while(n < this->N_MAX){
    Solution sol2 = sol;


    bool force_next = shake(sol2, n, c[n]);

    //this->VND(sol2);
    this->ls->bestImprovement(sol2);

    this->neighborhoodChange(sol, sol2, c[n], n, force_next);

    gettimeofday(&stop, NULL);
    double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    if(elapsed > timeout){
  	cout << "TIMEOUT" << endl;
  	break;
    }

    cout << fixed << "solucao atual " << sol.value << endl;
  }
}
