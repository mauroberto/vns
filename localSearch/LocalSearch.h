#ifndef LOCALSEARCH_H
#define LOCALSEARCH_H

#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "Neighborhood.h"
#include "Instance.h"
#include "Solution.h"
#include <time.h>
#include <sys/time.h>
#include "Movement.h"

class LocalSearch {
private:
  std::vector<Neighborhood*> neighbors;
  struct timeval start;
  int timeout; 
public:
  Instance* ins;

  LocalSearch(Instance* ins, int timeout, timeval start);
  LocalSearch();
  ~LocalSearch();

  void BestImprovement(Solution & sol);
};

#endif
