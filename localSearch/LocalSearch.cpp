#include "LocalSearch.h"

using namespace std;

#ifndef PATHSOL
#define PATHSOL "localSearch/solutions"
#endif

LocalSearch::LocalSearch(){}

LocalSearch::LocalSearch(Instance* ins, int timeout, timeval start) {
  this->ins = ins;

  //número de vizinhanças
  this->neighbors.resize(4);
  this->timeout = timeout;
  this->start = start;


  //instanciando as vizinhanças - pode ser feito por parâmetro
  this->neighbors[0] = new Move(this->ins);
  this->neighbors[1] = new Repack(this->ins);
  this->neighbors[2] = new AddItem(this->ins);
  this->neighbors[3] = new ChangeItem(this->ins);
}

LocalSearch::~LocalSearch() { 
  int s = this->neighbors.size();
  for(int i = 0; i < s; i++){
    delete this->neighbors[i];
  }
}

void LocalSearch::BestImprovement(Solution & sol){
  double v, v2;
  int s = this->neighbors.size();
  int space;
  struct timeval stop;
  Solution max;
  do{
    max = sol;
    for(int i=0; i<s; i++){
      this->neighbors[i]->bestImprovement(sol);
    }
  } while (sol > max);

  sol = max;
}
